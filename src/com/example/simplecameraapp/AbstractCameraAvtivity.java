package com.example.simplecameraapp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.view.Display;
import android.view.SurfaceView;
import android.view.WindowManager;

abstract class AbstractCameraAvtivity extends Activity implements CameraActivityInterface{

    protected static final int MIN_PREVIEW_PIXELS = 470 * 320;
    protected static final int MAX_PREVIEW_PIXELS = 1280 * 720;
    protected Camera mCamera;
    protected SurfaceView mSurfaceView;
    protected Point mPreviewSize;
    protected float mPreviewWidthRatio;
    protected float mPreviewHeightRatio;
    
    protected void setPreviewSize() {
        Camera.Parameters parameters = mCamera.getParameters();
        List<Size> rawPreviewSizes = parameters.getSupportedPreviewSizes();
        List<Size> supportPreviewSizes = new ArrayList<Size>(rawPreviewSizes);
        Collections.sort(supportPreviewSizes, new Comparator<Size>() {
            @Override
            public int compare(Size lSize, Size rSize) {
                int lPixels = lSize.width * lSize.height;
                int rPixels = rSize.width * rSize.height;
                if (rPixels < lPixels) {
                    return -1;
                }
                if (rPixels > lPixels) {
                    return 1;
                }
                return 0;
            }
        });
        WindowManager manager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        int screenWidth = display.getWidth();
        int screenHeight = display.getHeight();
        float screenAspectRatio = (float) screenWidth / (float) screenHeight;
        Point bestSize = null;
        float diff = Float.POSITIVE_INFINITY;
        for (Size supportPreviewSize : supportPreviewSizes) {
            int supportWidth = supportPreviewSize.width;
            int supportHeight = supportPreviewSize.height;
            int pixels = supportWidth * supportHeight;
            if (pixels < MIN_PREVIEW_PIXELS || pixels > MAX_PREVIEW_PIXELS) {
                continue;
            }
            boolean isPortrait = supportWidth < supportHeight;
            int previewWidth = isPortrait ? supportHeight : supportWidth;
            int previewHeight = isPortrait ? supportWidth : supportHeight;
            if (previewWidth == screenWidth && previewHeight == screenHeight) {
                mPreviewSize = new Point(supportWidth, supportHeight);
                mPreviewWidthRatio = 1;
                mPreviewHeightRatio = 1;
                return;
            }
            float aspectRatio = (float) previewWidth / (float) previewHeight;
            float newDiff = Math.abs(aspectRatio - screenAspectRatio);
            if (newDiff < diff) {
                bestSize = new Point(supportWidth, supportHeight);
                diff = newDiff;
            }
        }
        if (bestSize == null) {
            Size defaultSize = parameters.getPreviewSize();
            bestSize = new Point(defaultSize.width, defaultSize.height);
        }
        mPreviewSize = bestSize;
        mPreviewWidthRatio = (float) mPreviewSize.x / (float) screenWidth;
        mPreviewHeightRatio = (float) mPreviewSize.y / (float) screenHeight;
    }
}
