package com.example.simplecameraapp;

import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.PreviewCallback;
import android.view.SurfaceHolder;

/**
 * カメラアクティビティのインターフェース。
 * 
 * @author shugo
 * 
 */
public interface CameraActivityInterface extends SurfaceHolder.Callback,
        AutoFocusCallback, PreviewCallback {

}
