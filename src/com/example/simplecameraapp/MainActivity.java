package com.example.simplecameraapp;

import java.io.IOException;
import android.hardware.Camera;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ToggleButton;

public class MainActivity extends AbstractCameraAvtivity{

    private final String TAG = "SampleQrActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initCamera();
        

        ToggleButton tb = (ToggleButton) findViewById(R.id.toggleButton1);
        tb.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                    boolean isChecked) {
                if (isChecked){
                    //mFlashLight.setFlashLightOn(FlashLight.LIGHT_COLOR_WHITE);
                    Camera.Parameters parameters = mCamera.getParameters();
                    parameters.setFlashMode("torch");
                    mCamera.setParameters(parameters);
                    Log.d("flash", "torch");
                }else{
                    //mFlashLight.setFlashLightOff();
                    Camera.Parameters parameters = mCamera.getParameters();
                    parameters.setFlashMode("off");
                    mCamera.setParameters(parameters);
                    Log.d("flash", "off");
                }
                
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCamera = Camera.open();
        setPreviewSize();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            if (mCamera != null) {
                mCamera.setPreviewDisplay(holder);
            }
        } catch (IOException exception) {
            Log.e(TAG, "IOException caused by setPreviewDisplay()", exception);
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if (mCamera != null) {
            Camera.Parameters parameters = mCamera.getParameters();
            parameters.setPreviewSize(mPreviewSize.x, mPreviewSize.y);
            mCamera.setParameters(parameters);
            mCamera.startPreview();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (mCamera != null) {
            mCamera.stopPreview();
        }
    }

    private void initCamera() {
        mSurfaceView = (SurfaceView) findViewById(R.id.preview);
        SurfaceHolder holder = mSurfaceView.getHolder();
        holder.addCallback(this);
    }


    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
        
    }

    @Override
    public void onAutoFocus(boolean success, Camera camera) {
        if (success) {
            mCamera.setOneShotPreviewCallback(this);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (mCamera != null) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                mCamera.autoFocus(this);
            }
        }
        return super.onTouchEvent(event);
    }
}